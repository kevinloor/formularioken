const formulario = document.querySelector('form');
const nombre = document.querySelector('#nombre');
const email = document.querySelector('#email');
const enviar = document.querySelector('#enviar');
const cedula = document.querySelector('#cedula');

formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    
    if (nombre.value === '') {
        alert('Por favor, ingrese su nombre.');
        nombre.focus();
        return false;
    }
    
    if (email.value === '') {
        alert('Por favor, ingrese su email.');
        email.focus();
        return false;
    }
    
    // Validar el formato del email
    const expresion = /\S+@\S+\.\S+/;
    if (!expresion.test(email.value)) {
        alert('Por favor, ingrese un email válido.');
        email.focus();
        return false;
    }

    if (!expresion.test(cedula.value)){
        alert('por favor ingrse numeros');
        cedula.focus();
        return false;
    }
    
    alert('Formulario enviado con éxito!');
    formulario.reset();
});
